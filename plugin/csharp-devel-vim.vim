" I found this C++ devel helper vim script file on the KDE Tech Base 
" website which I used to help create a quick C# development helper
" script. The link is below my comment but I will also include the 
" descrition given in case the website goes down later.
"
"
" -- Begin Quote --"{{{
"Vim
"
" You can find a vim script in kde-devel-vim.vim that helps you to keep
" the coding style correct. In addition to defaulting to the kdelibs
" coding style it will automatically use the correct style for Solid and
" kdepim code. If you want to add rules for other projects feel free to
" add them in the SetCodingStyle function.
"
" To use the script, include it in your ~/.vimrc like this:
"
" source /path/to/kde/sources/kdesdk/scripts/kde-devel-vim.vim
"
" Document started by Urs Wolfer. Some parts of this document have been
" adopted from the Qt Coding Style document posted by Zack Rusin on
" kde-core-devel.
" -- End Quote --"}}}
"
" LINK:  http://techbase.kde.org/Policies/Kdelibs_Coding_Style#Vim
"
" John Kaul
"

"To use this file, add this line to your ~/.vimrc:, w/o the dquote
"source /path/to/scripts/csharp-devel-vim.vim
"
" For CreateChangeLogEntry() : If you don't want to re-enter your
" Name/Email in each vim session then make sure to have the viminfo
" option enabled in your ~/.vimrc, with the '!' flag, enabling
" persistent storage of global variables. Something along the line of
" set   viminfo=%,!,'50,\"100,:100,n~/.viminfo should do the trick.
"

"" " If TagList is Loaded then get a funny statusline
"" " Only works if kde-devel-vim.vim is loaded after taglist.
"" " Droping this script in ~/.vim/plugin works fine
"" if exists('loaded_taglist')
""     let Tlist_Process_File_Always=1
""     set statusline=%<%f:[\ %{Tlist_Get_Tag_Prototype_By_Line()}\ ]\ %h%m%r%=%-14.(%l,%c%V%)\ %P
"" endif

" Insert tab character in whitespace-only lines, complete otherwise
inoremap <Tab> <C-R>=SmartTab()<CR>

if !exists("DisableSmartParens")
" Insert a space after ( or [ and before ] or ) unless preceded by a matching
" paren/bracket or space or inside a string or comment. Comments are only
" recognized as such if they start on the current line :-(
inoremap ( <C-R>=SmartParens( '(' )<CR>
inoremap [ <C-R>=SmartParens( '[' )<CR>
inoremap ] <C-R>=SmartParens( ']', '[' )<CR>
inoremap ) <C-R>=SmartParens( ')', '(' )<CR>
endif

" Insert a forward declaration for the current/last symbol
inoremap <S-F5> <C-O>:call AddForward()<CR>

" Toggle line comments on Ctrl+\
map <C-Bslash> :call CommentLine()<LF>

" Project or standard C# comment block
imap <silent>  ///  <C-R>=CommentBlock(input("Enter comment: "), {'box':'=', 'width':73})<CR>

" Call AlignAssignments() for the current block of code.
nmap <silent>  ;=  :call AlignAssignments()<CR>

" Remap <TAB> for smart completion on various characters...
inoremap <silent> <TAB>   <C-R>=SmartComplete()<CR>

function! SetCodingStyle()     "{{{
    if &syntax == 'cmake'
        call SmartParensOff()
        set sw=3
        set ts=3
        set et
        set tw=0
        return
    endif
    if ( &syntax !~ '^\(cs\\)$' )
        set listchars=tab:?\ ,trail:?
        set list
        iab i i
        set incsearch
        "" Disable the following lines for now untill I can verify that compiler can be called with cmake/vim/make
        "" 
        "" set efm=%f:%l:\ %m,In\ file\ included\ from\ %f:%l:,\^I\^Ifrom\ %f:%l%m
        "" " The following few lines will allow out-of-source-builds; Essentially, we
        "" " search the directory structure for a `BIN' folder and then a
        "" " `BIN\Makefile' to direct `makeprg' where to call our compiler.
        ""    let $BINDIR = Directory_matcher()
        ""    cd $BINDIR
        "" " look for a folder bin up and down to set the current directory there
        "" " (for calling make).
        ""    let $MAKEFILE = findfile($BINDIR . "\\Makefile", ".;")
        "" " Look for a folder $BINDIR\Makefile up and down in the current locaion; used
        "" " for the makeprg setting below
        ""    set makeprg=make\ -f\ $MAKEFILE
        return
    endif
    "the path for the file
    let pathfn = expand( '%:p:h' )
        call SmartParensOff()
        inoremap ( <C-R>=SpaceBetweenKeywordAndParens()<CR>
        let g:need_brace_on_next_line = '\<\(class\|namespace\|struct\)\>'
        let g:need_brace_on_same_line = '\<\(if\|else\|while\|switch\|do\|foreach\|forever\|enum\|for\|try\|catch\)\>'
        set sw=4
        set sts=4
        set et
        set tw=100
    if ( !exists("g:noautobrace") )
        call EnableSmartLineBreak()
    endif
endfunction "}}}

function! DisableSmartLineBreak()     "{{{
    iunmap <CR>
    iuna else
endfunction "}}}

function! EnableSmartLineBreak()     "{{{
    if exists("*pumvisible")
        inoremap <CR> <C-R>=pumvisible() ? "\<lt>CR>" : "\<lt>ESC>:call SmartLineBreak()\<lt>CR>a\<lt>CR>"<CR>
    else
        inoremap <CR> <ESC>:call SmartLineBreak()<CR>a<CR>
    endif
    iab else <C-R>=SmartElse()<CR>
endfunction "}}}

function! SmartElse()     "{{{
    let prefix = ''
    if strlen(g:need_brace_on_same_line) > 0 && 'else' =~ g:need_brace_on_same_line
        if getline('.') =~ '^\s*$'
            if getline(line('.') - 1) =~ '}$'
                let prefix = prefix . "\<ESC>kmMjdd`MA "
            elseif getline(line('.') - 1) =~ '}\s*$'
                let prefix = prefix . "\<ESC>kmMjdd`MA"
            endif
        endif
    endif
    return prefix . "else\<Right>"
endfunction "}}}

function! CreateMatchLine()     "{{{
    let linenum = line( '.' )
    let current_line = getline( linenum )
    " don't do magic if the cursor isn't at the end of the line or if it's
    " inside a // comment
    if col( '.' ) != strlen( current_line ) || match( current_line, '//' ) >= 0
        return ''
    endif
    " remove whitespace at the end
    if match( current_line, '\s\+$' ) >= 0
        :execute ':s/\s*$//'
        " the following is needed if return '' is called
        :execute "normal $"
    endif
    let current_line = getline( linenum )
    " remove all /* */ comments
    let current_line = substitute( current_line, '/\*.\{-}\*/', '', 'g' )
    " remove all strings
    let current_line = substitute( current_line, "'[^']*'", '', 'g' )
    let current_line = substitute( current_line, '"\(\\"\|[^"]\)*"', '', 'g' )
    " remove all ( )
    while current_line =~ '(.*)'
        let current_line = substitute( current_line, '([^()]*)', '', 'g' )
    endwhile
    " prepend earlier lines until we find a ; or {
    while linenum > 1 && current_line !~ ';' && current_line !~ '{.\+$'
        let linenum = linenum - 1
        let prev_line = getline(linenum)
        if synIDattr(synID(linenum, 1, 1), "name") == 'cComment' "inside a /* */ comment at the beginning of the line
            if stridx(prev_line, '*/') == -1
                " next line please
                let prev_line = ''
            else
                " remove everything before */
                let prev_line = substitute(prev_line, '^.*\*/', '*/', '')
            endif
        endif
        " remove // comment
        let prev_line = substitute(prev_line, '//.*$', '', '' )
        " concatenate the lines with a space in between
        let current_line = prev_line.' '.current_line
        " remove all /* */ comments
        let current_line = substitute( current_line, '/\*.\{-}\*/', '', 'g' )
        " remove all strings
        let current_line = substitute( current_line, "'[^']*'", '', 'g' )
        let current_line = substitute( current_line, '"\(\\"\|[^"]\)*"', '', 'g' )
        " remove all ( )
        while current_line =~ '(.*)'
            let current_line = substitute( current_line, '([^()]*)', '', 'g' )
        endwhile
    endwhile
    " remove everything until the last ;
    let current_line = substitute( current_line, '^.*;', '', '' )
    " remove everything until the last { which is not at the end of the line
    let current_line = substitute( current_line, '^.*{\(.\+\)$', '\1', '' )
    " remove all [ ]
    while current_line =~ '\[.*\]'
        let current_line = substitute( current_line, '\[[^\[\]]*\]', '', 'g' )
    endwhile
    " if <CR> was pressed inside ( ), [ ] or /* */ don't add braces
    if current_line =~ '[(\[]' || current_line =~ '/\*'
        return ''
    endif
    return current_line
endfunction "}}}

function! AddClosingBrace(current_line)"{{{
    if a:current_line =~ '\<enum\|class\|struct\>'
        :execute "normal o};\<ESC>k"
    elseif a:current_line =~ '\<while\|if\|else\|for\|switch\|do\>'
        :execute "normal o}\<ESC>k"
    elseif a:current_line =~ '\<namespace\>'
        let namespace = substitute( a:current_line, '^.*namespace\s\+', '', '' )
        let namespace = substitute( namespace, '\s.*$', '', '' )
        :execute "normal o} // namespace " . namespace . "\<ESC>k"
    else
        :execute "normal o}\<ESC>k"
    endif
endfunction "}}}

function! SmartLineBreak()     "{{{
    if synIDattr(synID(line("."), col("."), 1), "name") == 'cComment' "inside a /* */ comment at the point where the line break occurs
        return
    endif
    let match_line = CreateMatchLine()
    if match_line == ''
        return
    endif

    let match_position1 = -1
    let match_position2 = -1
    if strlen(g:need_brace_on_same_line) > 0
        let match_position1 = match(match_line, g:need_brace_on_same_line)
        if match_position1 > 0
            while strpart(match_line, match_position1 - 1, 1) == '#'
                let old_position = match_position1
                let match_position1 = match(match_line, g:need_brace_on_same_line, match_position1 + 1)
                if match_position1 == -1
                    if strpart(match_line, old_position, 2) == 'if'
                        :execute "normal o#endif\<ESC>k$"
                    endif
                    return
                endif
            endwhile
        endif
    endif
    if strlen(g:need_brace_on_next_line) > 0 && match_position1 == -1
        let match_position2 = match(match_line, g:need_brace_on_next_line)
        if match_position2 > 0
            while strpart(match_line, match_position2 - 1, 1) == '#'
                let old_position = match_position2
                let match_position2 = match(match_line, g:need_brace_on_same_line, match_position2 + 1)
                if match_position2 == -1
                    if strpart(match_line, old_position, 2) == 'if'
                        :execute "normal o#endif\<ESC>k$"
                    endif
                    return
                endif
            endwhile
        endif
    endif

    if match_position1 > -1
        if match_line =~ '}\s*else\>'
            " make sure else is on the same line as the closing brace
            if getline('.') =~ '^\s*else'
                if getline(line('.') - 1) =~ '}$'
                    :execute "normal kA \<ESC>J"
                elseif getline(line('.') - 1) =~ '}\s*$'
                    :execute "normal kJ"
                endif
            endif
        endif
        while getline('.') =~ '^\s*{$'
            " opening brace is on its own line: move it up
            :execute "normal kJ"
        endwhile
        if match_line =~ '{$'
            if getline('.') =~ '[^ ]{$'
                :execute ':s/{$/ {/'
            endif
        else
            :execute ':s/$/ {/'
        endif
        call AddClosingBrace(match_line)
    elseif getline('.') =~ '^\s*{$'
        call AddClosingBrace('')
    elseif match_position2 > -1
        if match_line =~ '{$'
            :execute ':s/\s*{$//'
        endif
        :execute "normal o{"
        call AddClosingBrace(match_line)
    endif
    :execute "normal $"
endfunction "}}}

function! SmartParensOn()     "{{{
    inoremap ( <C-R>=SmartParens( '(' )<CR>
    inoremap [ <C-R>=SmartParens( '[' )<CR>
    inoremap ] <C-R>=SmartParens( ']', '[' )<CR>
    inoremap ) <C-R>=SmartParens( ')', '(' )<CR>
endfunction "}}}

function! SmartParensOff()     "{{{
    if strlen(mapcheck('[','i')) > 0
        iunmap (
        iunmap [
        iunmap ]
        iunmap )
    endif
endfunction "}}}

function! SmartTab()     "{{{
    let col = col('.') - 1
    if !col || getline('.')[col-1] !~ '\k'
        return "\<Tab>"
    else
        return "\<C-P>"
    endif
endfunction "}}}

function! SmartParens( char, ... )      "{{{
    if ! ( &syntax =~ '^\(cs\)$' )
        return a:char
    endif
    let s = strpart( getline( '.' ), 0, col( '.' ) - 1 )
    if s =~ '//'
        return a:char
    endif
    let s = substitute( s, '/\*\([^*]\|\*\@!/\)*\*/', '', 'g' )
    let s = substitute( s, "'[^']*'", '', 'g' )
    let s = substitute( s, '"\(\\"\|[^"]\)*"', '', 'g' )
    if s =~ "\\([\"']\\|/\\*\\)"
        return a:char
    endif
    if a:0 > 0
        if strpart( getline( '.' ), col( '.' ) - 3, 2 ) == a:1 . ' '
            return "\<BS>" . a:char
        endif
        if strpart( getline( '.' ), col( '.' ) - 2, 1 ) == ' '
            return a:char
        endif
        return ' ' . a:char
    endif
    if !exists("g:DisableSpaceBeforeParen")
        if a:char == '('
            if strpart( getline( '.' ), col( '.' ) - 3, 2 ) == 'if' ||
              \strpart( getline( '.' ), col( '.' ) - 4, 3 ) == 'for' ||
              \strpart( getline( '.' ), col( '.' ) - 6, 5 ) == 'while' ||
              \strpart( getline( '.' ), col( '.' ) - 7, 6 ) == 'switch'
                return ' ( '
            endif
        endif
    endif
    return a:char . ' '
endfunction "}}}

function! SpaceBetweenKeywordAndParens()     "{{{
    if ! ( &syntax =~ '^\(c\|cpp\|java\)$' )
        return '('
    endif
    let s = strpart( getline( '.' ), 0, col( '.' ) - 1 )
    if s =~ '//'
        " text inside a comment
        return '('
    endif
    let s = substitute( s, '/\*\([^*]\|\*\@!/\)*\*/', '', 'g' )
    let s = substitute( s, "'[^']*'", '', 'g' )
    let s = substitute( s, '"\(\\"\|[^"]\)*"', '', 'g' )
    if s =~ "\\([\"']\\|/\\*\\)"
        " text inside a string
        return '('
    endif
    if a:0 > 0
        if strpart( getline( '.' ), col( '.' ) - 3, 2 ) == a:1 . ' '
            return "\<BS>" . a:char
        endif
        if strpart( getline( '.' ), col( '.' ) - 2, 1 ) == ' '
            return a:char
        endif
        return ' ' . a:char
    endif
    if strpart( getline( '.' ), col( '.' ) - 3, 2 ) == 'if' ||
        \strpart( getline( '.' ), col( '.' ) - 4, 3 ) == 'for' ||
        \strpart( getline( '.' ), col( '.' ) - 6, 5 ) == 'while' ||
        \strpart( getline( '.' ), col( '.' ) - 7, 6 ) == 'switch' ||
        \strpart( getline( '.' ), col( '.' ) - 8, 7 ) == 'foreach' ||
        \strpart( getline( '.' ), col( '.' ) - 8, 7 ) == 'forever'
        return ' ('
    endif
    return '('
endfunction "}}}

function! AddForward()     "{{{
    let s = getline( '.' )
    let i = col( '.' ) - 1
    while i > 0 && strpart( s, i, 1 ) !~ '[A-Za-z0-9_:]'
        let i = i - 1
    endwhile
    while i > 0 && strpart( s, i, 1 ) =~ '[A-Za-z0-9_:]'
        let i = i - 1
    endwhile
    let start = match( s, '[A-Za-z0-9_]\+\(::[A-Za-z0-9_]\+\)*', i )
    let end = matchend( s, '[A-Za-z0-9_]\+\(::[A-Za-z0-9_]\+\)*', i )
    if end > col( '.' )
        let end = matchend( s, '[A-Za-z0-9_]\+', i )
    endif
    let ident = strpart( s, start, end - start )
    let forward = 'class ' . ident . ';'

    let line = 1
    let incomment = 0
    let appendpos = 0
    let codestart = 0
    while line <= line( '$' )
        let s = getline( line )
        if incomment == 1
            let end = matchend( s, '\*/' )
            if end == -1
                let line = line + 1
                continue
            else
                let s = strpart( s, end )
                let incomment = 0
            endif
        endif
        let s = substitute( s, '//.*', '', '' )
        let s = substitute( s, '/\*\([^*]\|\*\@!/\)*\*/', '', 'g' )
        if s =~ '/\*'
            let incomment = 1
        elseif s =~ '^' . forward
            break
        elseif s =~ '^\s*class [A-za-z0-9_]\+;' || (s =~ '^#include' && s !~ '\.moc"')
            let appendpos = line
        elseif codestart == 0 && s !~ '^$'
            let codestart = line
        endif
        let line = line + 1
    endwhile
    if line == line( '$' ) + 1
        if appendpos == 0
            call append( codestart - 1, forward )
            call append( codestart, '' )
        else
            call append( appendpos, forward )
        endif
    endif
endfunction  "  }}}

function! CreateChangeLogEntry()     "{{{
    let currentBuffer = expand( "%" )

    if exists( "g:EMAIL" )
        let mail = g:EMAIL
    elseif exists( "$EMAIL" )
        let mail = $EMAIL
    else
        let mail = inputdialog( "Enter Name/Email for Changelog entry: " )
    if mail == ""
        echo "Aborted ChangeLog edit..."
        return
    endif
    let g:EMAIL = mail
    endif

    if bufname( "ChangeLog" ) != "" && bufwinnr( bufname( "ChangeLog" ) ) != -1
    execute bufwinnr( bufname( "ChangeLog" ) ) . " wincmd w"
    else
        execute "split ChangeLog"
    endif

    let lastEntry = getline( nextnonblank( 1 ) )
    let newEntry = strftime("%Y-%m-%d") . "  " . mail

    if lastEntry != newEntry
        call append( 0, "" )
        call append( 0, "" )
        call append( 0, newEntry )
    endif

    " like emacs, prepend the current buffer name to the entry. but unlike
    " emacs I have no idea how to figure out the current function name :(
    " (Simon)
    if currentBuffer != ""
        let newLine = "\t* " . currentBuffer . ": "
    else
        let newLine = "\t* "
    endif

    call append( 2, newLine )

    execute "normal 3G$"
endfunction "}}}

function! CreateTODOEntry()     "{{{
    let currentBuffer = expand( "%" )

    if bufname( "TODO" ) != "" && bufwinnr( bufname( "TODO" ) ) != -1
    execute bufwinnr( bufname( "TODO" ) ) . " wincmd w"
    else
        execute "split TODO"
    endif

    let newLine = "* [ ] ( " . currentBuffer . " ): "
    call append( line('$'), newLine )
    execute "normal 3G$"
endfunction "}}}

function! Directory_matcher()       "{{{
    "List of troublesome words...
    let s:directories = [
                \ "bin",  "build",
                \ "binary",  "Development",
                \ "Debug",  "release"
                \ ]

    for $dir in s:directories
        if finddir($dir, ",;") != ""
            return finddir($dir, ".;")
        endif
    endfor
    return "."
endfunction "}}}

function! AlignAssignments ()        "{{{
    "   Align Assignments:
    "   This function will align the assignments for the statements in a code
    "   block.
    "
    "   Example:
    "     applicants_name = 'Luke'
    "     mothers_maiden_name = 'Amidala'
    "     closest_relative = 'sister'
    "     fathers_occupation = 'Sith'
    "
    "     applicants_name     = 'Luke'
    "     mothers_maiden_name = 'Amidala'
    "     closest_relative    = 'sister'
    "     fathers_occupation  = 'Sith'
    "
    "     Code for this function found here:
    "     http://www.ibm.com/developerworks/linux/library/l-vim-script-2/index.html
    "

    " Patterns needed to locate assignment operators...
    let ASSIGN_OP   = '[-+*/%|&]\?=\@<!=[=~]\@!'
    let ASSIGN_LINE = '^\(.\{-}\)\s*\(' . ASSIGN_OP . '\)\(.*\)$'

    " Locate block of code to be considered (same indentation, no blanks)...
    let indent_pat = '^' . matchstr(getline('.'), '^\s*') . '\S'
    let firstline  = search('^\%('. indent_pat . '\)\@!','bnW') + 1
    let lastline   = search('^\%('. indent_pat . '\)\@!', 'nW') - 1
    if lastline < 0
        let lastline = line('$')
    endif

    " Decompose lines at assignment operators...
    let lines = []
    for linetext in getline(firstline, lastline)
        let fields = matchlist(linetext, ASSIGN_LINE)
        if len(fields)
            call add(lines, {'lval':fields[1], 'op':fields[2], 'rval':fields[3]})
        else
            call add(lines, {'text':linetext,  'op':''                         })
        endif
    endfor

    " Determine maximal lengths of lvalue and operator...
    let op_lines = filter(copy(lines),'!empty(v:val.op)')
    let max_lval = max( map(copy(op_lines), 'strlen(v:val.lval)') ) + 1
    let max_op   = max( map(copy(op_lines), 'strlen(v:val.op)'  ) )

    " Recompose lines with operators at the maximum length...
    let linenum = firstline
    for line in lines
        let newline = empty(line.op)
        \ ? line.text
        \ : printf("%-*s%*s%s", max_lval, line.lval, max_op, line.op, line.rval)
        call setline(linenum, newline)
        let linenum += 1
    endfor
endfunction     "}}}

function! CommentLine()     "{{{
  if getline(".") =~ '//-x-   '
    let hls=@/
    s,^//-x-   ,,
    let @/=hls
  else
    let hls=@/
    s,^,//-x-   ,
    let @/=hls
  endif
endfunction "}}}

function! CommentBlock(comment, opt)        "{{{
    " Unpack optional arguments...
    let introducer = get(a:opt, 'intro', '/// '               )
    let box_char   = get(a:opt, 'box',   '*'                  )
    let width      = get(a:opt, 'width', strlen(a:comment) + 2)" Build the comment box and put the comment inside it...
    return introducer . repeat(box_char,width) . "\<CR>"
    \    . "/" . " " . a:comment        . "\<CR>"
    \    . "/" . " \\brief:" . "\<CR>"
    \    . "/" . "\<CR>"
    \    . "/" . " " . repeat(box_char,width) . "\<CR>"
endfunction     "}}}

" Implement smart completion magic...
function! SmartComplete ()   "{{{
    " Remember where we parked...
    let cursorpos = getpos('.')
    let cursorcol = cursorpos[2]
    let curr_line = getline('.')

    " Special subpattern to match only at cursor position...
    let curr_pos_pat = '\%' . cursorcol . 'c'

    " Tab as usual at the left margin...
    if curr_line =~ '^\s*' . curr_pos_pat
        return "\<TAB>"
    endif

    " How to restore the cursor position...
    let cursor_back = "\<C-O>:call setpos('.'," . string(cursorpos) . ")\<CR>"

    " If a matching smart completion has been specified, use that...
    for [left, right, completion, restore] in s:completions
        let pattern = left . curr_pos_pat . right
        if curr_line =~ pattern
            " Code around bug in setpos() when used at EOL...
            if cursorcol == strlen(curr_line)+1 && strlen(completion)==1
                let cursor_back = "\<LEFT>"
            endif

            " Return the completion...
            return completion . (restore ? cursor_back : "")
        endif
    endfor

    " If no contextual match and after an identifier, do keyword completion...
    if curr_line =~ '\k' . curr_pos_pat
        return "\<C-N>"

    " Otherwise, just be a <TAB>...
    else
        return "\<TAB>"
    endif
endfunction     "}}}

" Table of completion specifications (a list of lists)...
let s:completions = []
function! AddCompletion (left, right, completion, restore)
    " Function to add user-defined completions...
    call insert(s:completions, [a:left, a:right, a:completion, a:restore])
endfunction
let s:NONE = ""
" Table of completions...
"                    Left           Right       Complete with...            Restore
"                    =====          =======     ====================        =======
call AddCompletion(  '{',           s:NONE,     "}",                        1   )
call AddCompletion(  '{',           '}',        "\<CR>\<C-D>\<ESC>O\<TAB>", 0   )
call AddCompletion(  '\[',          s:NONE,     " ]",                       1   )
call AddCompletion(  '\[',          ' \]',       "\<CR>\<ESC>O\<TAB>",      0   )
call AddCompletion(  '(',           s:NONE,     " )",                       1   )
call AddCompletion(  '(',           ' )',        "\<CR>\<ESC>O\<TAB>",      0   )
call AddCompletion(  '<',           s:NONE,     ">",                        1   )
call AddCompletion(  '<',           '>',        "\<CR>\<ESC>O\<TAB>",       0   )
call AddCompletion(  '"',           s:NONE,     '"',                        1   )
call AddCompletion(  '"',           '"',        "\\n",                      1   )
call AddCompletion(  "'",           s:NONE,     "'",                        1   )
call AddCompletion(  "'",           "'",        s:NONE,                     0   )

" ================================
" Autogroup settings.
" ================================
augroup CSharpProgramming
    au!
    au BufNewFile,BufRead,BufEnter *.cs filetype indent on
    " automatic indenting is required for SmartLineBreak to work correctly
    au BufRead,BufNewFile,BufEnter *.cs :call SetCodingStyle()
augroup END

" vim: sw=4 sts=4 et
"
