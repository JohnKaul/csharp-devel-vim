# README #

This Vim script _(adopted from `kde-devel-vim`)_ is generic C# helper script. I want to give full credit to the original author(s) of the KDE-Cpp-helper-script so I will add a header in the script file with a link to the original.

### How do I get set up? ###
If you don't have a preferred installation method, I recommend installing [pathogen.vim](https://github.com/tpope/vim-pathogen), and then simply copy and paste:

    cd ~/.vim/bundle
    git clone https://JohnKaul@bitbucket.org/JohnKaul/csharp-devel-vim.git

Otherwise you can always download this and place the `csharp-devel-vim.vim` file in the `plugin` directory. 

### Contribution guidelines ###

* Contribute? Please. Feel free.
* Code review? Yes, please.
* Comments? Yes, please.

### Who do I talk to? ###

* John Kaul - john.kaul@outlook.com
